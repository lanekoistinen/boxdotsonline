import java.awt.Color;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.ServerSocket;
import java.net.Socket;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JRadioButton;



/**
 * Dots
 * Java version of the dots paper game
 * 05.28.2020
 * @author LaneKoistinen
 *
 */
public class NetDot extends JFrame {

	boolean connected = false;
	
	/**
	 *  Declare UI elements
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private boolean Server = true;
	volatile int current_player = Constants.NONE;
	String str_p1, str_p2;
	JLabel score_1, score_2;
	GamePanel panel;
	private JTextField p1Field;
	private JTextField p2Field;
	private volatile JLabel lblName1;
	private volatile JLabel lblName2;
	private JLabel lblCurrentPlayer;
	private JTextField tfHost;
	private JRadioButton rdbtnClient;
	private JRadioButton rdbtnServer;
	private JButton btnNewGame;
	netThread net;

	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					// Create the class
					NetDot frame = new NetDot();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				
			}
		});		
	}

	
	
	/**
	 * NetDot
	 * 
	 * Constructor creates the main frame and constructs the various UI
	 * elements
	 */
	
	public NetDot() {
		
		// Default frame settings
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 595, 534);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		// Set content
		setContentPane(contentPane);
		
		// Set default layout and initialize background color
		// to represent player 1
		contentPane.setLayout(null);
		contentPane.setBackground(Constants.COLOR_P1_BKGD);
		
		// Outline dialog
		contentPane.setBorder(BorderFactory.createLineBorder(Color.black));
		
		// Configure Player 1 score UI
		score_1 = new JLabel("0");
		score_1.setBounds(23, 29, 73, 20);
		contentPane.add(score_1);
		
		// Configure Player 2 score UI
		score_2 = new JLabel("0");
		score_2.setBounds(86, 33, 73, 14);
		contentPane.add(score_2);
		
		// Initialize panel by calling a new game
		new_game();
		
		// Initialize Player 1 label to default
		JLabel player_1_label = new JLabel("Player 1:");
		player_1_label.setBounds(462, 91, 73, 14);
		contentPane.add(player_1_label);
		
		// Initialize Player 2 label to default
		JLabel player_2_label = new JLabel("Player 2:");
		player_2_label.setBounds(460, 359, 73, 14);
		contentPane.add(player_2_label);
		
		// Create the start button listen for mouse clicks 
		btnNewGame = new JButton("Start!");
		btnNewGame.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				// Retrieve user inputed names, default to server or client
				str_p1 = get_p1();
				str_p2 = get_p2();
				
				// Check connection status
				if ( !connected ) {
					// Use the server connection method or get the udpated host to use in the client connection method
				 	if ( ( connected = ( Server ) ? Connect_Server() : Connect_Client(tfHost.getText()) ) ) {
				 		
				 		// Update GUI for game-play
			 			btnNewGame.setText("Quit!");
			 			
			 			// Start the game
			 			clear_game();
				 	}
				 	// Connection error detected from client or servor connection method, administer error message
				 	else
				 			JOptionPane.showMessageDialog(null, "Error: Connection Failed -- Check Host" );
				 }
				// Reset game if quit is pressed (System exits by now in current version)
				else {
					 btnNewGame.setText("Start!");
					 Disconnect();
				}
			}
		});
		btnNewGame.setBounds(458, 214, 117, 23);
		contentPane.add(btnNewGame);
		btnNewGame.setEnabled(false);
		
		// Configure Player 1 name input field
		p1Field = new JTextField();
		p1Field.setBounds(464, 115, 115, 20);
		contentPane.add(p1Field);
		p1Field.setColumns(10);
		
		// Configure Player 2 name input field
		p2Field = new JTextField();
		p2Field.setBounds(460, 384, 115, 20);
		contentPane.add(p2Field);
		p2Field.setColumns(10);
		
		// Configure Player 1 name label
		lblName1 = new JLabel("Player 1");
		lblName1.setFont(new Font("Tahoma", Font.ITALIC, 11));
		lblName1.setBounds(23, 16, 73, 14);
		contentPane.add(lblName1);
		
		// Configure Player 2 name label
		lblName2 = new JLabel("Player 2");
		lblName2.setFont(new Font("Tahoma", Font.ITALIC, 11));
		lblName2.setBounds(86, 16, 73, 14);
		contentPane.add(lblName2);
		
		// Configure current player label
		lblCurrentPlayer = new JLabel(" ");
		lblCurrentPlayer.setFont(new Font("Tahoma", Font.BOLD, 17));
		lblCurrentPlayer.setBounds(307, 22, 141, 29);
		contentPane.add(lblCurrentPlayer);
		
		// Configure host text field
		tfHost = new JTextField();
		tfHost.setToolTipText("localhost");
		tfHost.setBounds(458, 430, 117, 20);
		contentPane.add(tfHost);
		tfHost.setColumns(10);
		
		// Configure client radio button
		rdbtnClient = new JRadioButton("Client");
		rdbtnClient.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				if ( rdbtnClient.isSelected() ) {
					
					btnNewGame.setText("Connect!");
					btnNewGame.setEnabled(true);
					p1Field.setEnabled(false);
					p2Field.setEnabled(true);
					tfHost.setEnabled(true);
					Server = false;
				}
			}
		});
		rdbtnClient.setBackground(Color.WHITE);
		rdbtnClient.setBounds(454, 329, 109, 23);
		contentPane.add(rdbtnClient);
		
		// Configure server radio button
		rdbtnServer = new JRadioButton("Server");
		rdbtnServer.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				if ( rdbtnServer.isSelected() ) {
					
					btnNewGame.setText("Start!");
					btnNewGame.setEnabled(true);
					p1Field.setEnabled(true);
					p2Field.setEnabled(false);
					tfHost.setEnabled(false);
					Server = true;
				}
			}
		});
		rdbtnServer.setBackground(Color.WHITE);
		rdbtnServer.setBounds(458, 60, 109, 23);
		contentPane.add(rdbtnServer);
		
		// Link buttons
		ButtonGroup G = new ButtonGroup();
		G.add(rdbtnServer);
		G.add(rdbtnClient);
		
		JLabel lblHost = new JLabel("Host:");
		lblHost.setBounds(458, 415, 48, 14);
		contentPane.add(lblHost);
	}
	
	
	
	/**
	 * set_name
	 * 
	 * Receives the connected player's name
	 * @param name update GUI name
	 */
	
	public synchronized void set_name(String name) {
		
		// Update server name
		if ( Server ) {
			
			// server is considered to be player 1,
			// so the connected player must be player 2
			str_p2 = name;
			panel.name_2 = name;
			p2Field.setText(name);
			lblName2.setText(name);
		}
		// Update client name
		else {
			
			// client is considered to be player 2,
			// so the connected player must be player 1
			str_p1 = name;
			lblCurrentPlayer.setText(name);
			panel.name_1 = name;
			p1Field.setText(name);
			lblName1.setText(name);
		}
	}
	
	
	
	/**
	 * Connect_Client
	 * 
	 * Client connection method
	 * @param host
	 * @return connection status
	 */
	
	public boolean Connect_Client(String host) {
		
		Socket sock;
		
		// Initialize socket to pass,
		// This will NOT be done in the thread, as it
		// does not require a loop and provides us with
		// an immediate connection status
		try {
			sock = new Socket(host, Constants.PORT);
		} catch (Exception e) {
			// Flag a connection error upon exception
			return false;
		}
		
		// Construct netThread class and pass parameters
		net = new netThread(this);
		net.Config(null, str_p2, sock);
		
		// Run thread
		net.start();
		
		return true;
	}
	
	
	
	/**
	 * Connect_Server
	 * 
	 * Server connection method
	 * @return
	 */
	
	public boolean Connect_Server() {
		
		ServerSocket ss;

		// Create server socket, this is NOT
		// done in the network thread, as it does not
		// require a loop and provides us with some
		// immediate connection feedback
	    try{
	    	ss=new ServerSocket(Constants.PORT);
	    } catch (Exception e){
	    	// Flag connection error upon exception
	    	return false;
	    }
		
	    // Construct netThread class and pass parameters
		net = new netThread(this);
		net.Config(ss, str_p1, null);
		
		// Run thread
		net.start();
		
		return true;
	}
	
	
	
	/**
	 * Disconnect
	 * 
	 * Calls the netThread's disconnect method
	 * and updates connection flag and GUI
	 */
	
	public void Disconnect() {
		
		if ( net != null )
			net.Disconnect();
		btnNewGame.setText("Start!");
		connected = false;
	}
	
	
	
	/**
	 * new_game
	 * 
	 * Creates and initializes a game panel
	 * Function should only be called once
	 * @return new game panel object
	 */
	
	protected GamePanel new_game() {
		
		// Start with first player
		current_player = Constants.PLAYER_1;

		// Use default parameters until user inputs other information
		panel = new GamePanel( str_p1 = "Server", str_p2 = "Client", score_1, score_2);
		
		// Listen for clicks on the game board and pass to the GamePanel class object
		panel.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
				
				// Perform click first in GamePanel, and then in Dots class
				if ( net == null)
					return;
				
				// Determine the player
				int player;
				if ( Server )
					player = Constants.PLAYER_1;
				else 
					player = Constants.PLAYER_2;
				
				// Panel ignores clicks when it is not the player's turn
				if ( player != current_player )
					return;
				
				// Send coordinates to other player via network class
				net.send_line("C " + e.getX() + " " + e.getY());
				
				// Respond to click internally
				if ( Server )
					on_Click(e.getX(), e.getY());
				else
					on_Click(e.getX(), e.getY());
			}
		});
		
		// Configure UI settings for GamePanel object
		panel.setBounds(10, 60, 10 + 50 * Constants.width + 2 * Constants.padding, 50 * Constants.height + 2 * Constants.padding);
		contentPane.add(panel);
	
		return panel;
	}
	
	
	
	/**
	 * on_Click
	 * 
	 * Respond to GamePanel clicks
	 * @param x x-coordinate of click
	 * @param y y-coordinate of click
	 */
	
	public synchronized void on_Click(int x, int y) {
		
		game_finish_click(panel.on_click(x, y, current_player));
	}
	
	
	
	/**
	 * trigger_exit
	 * 
	 * exit program
	 */
	public synchronized void trigger_exit() {
		
		System.exit(0);
	}
	
	
	
	/**
	 * clear_game
	 * 
	 * Resets the user input to create a restarted game
	 */
	
	public void clear_game() {
		
		// Start with player 1
		current_player = Constants.PLAYER_1;
		contentPane.setBackground(Constants.COLOR_P1_BKGD);
		
		// Clear input and update the score to get changes
		panel.clear_game();
		panel.update_score();
		
		// Retrieve player names
		str_p1 = get_p1();
		str_p2 = get_p2();
		
		// Display the current player (whose turn)
		lblCurrentPlayer.setText(( current_player == Constants.PLAYER_1 ) ? str_p1 : str_p2 );
		
		// Use the new names in GamePanel class object
		panel.name_1 = str_p1;
		panel.name_2 = str_p2;
		
		// Update the names on the UI
		lblName1.setText(str_p1);
		lblName2.setText(str_p2);
	}
	
	
	
	/**
	 * game_finish_click
	 * 
	 * Checks for a winner upon each click, uses the input to 
	 * conditionally advance to the next player's turn
	 * @param input check for valid input
	 */
	
	protected void game_finish_click( int input ) {
		
		// Check the score to see if the board is filled
		// and report the status of any win
		switch ( panel.check_winner() ) {
		
		case Constants.PLAYER_1:
			
			JOptionPane.showMessageDialog(null, str_p1 + " won!!!" );
			clear_game();
			break;
		case Constants.PLAYER_2:
			JOptionPane.showMessageDialog(null, str_p2 + " won!!!" );
			clear_game();
			break;
		case Constants.BOTH:
			JOptionPane.showMessageDialog(null, "TIE!!! ...boring");
			clear_game();
			break;
		default:
			break;
		}
		
		// Check if the user input should be ignored (ex: player gets another turn)
		if ( input != Constants.GOOD ) {
			
			// Check if the user input was a valid choice
			if ( input == Constants.NONE ) {
				
				JOptionPane.showMessageDialog(null, "Choose a valid spot!");
				return;
			}
			else
				return;
		}
		
		// Advance to the next player's turn based on the current player
		switch ( current_player ) {
		
		case Constants.PLAYER_1:
			
			// Display the UI appearance for the player
			// and adjust variable
			contentPane.setBackground(Constants.COLOR_P2_BKGD);
			current_player = Constants.PLAYER_2;
			lblCurrentPlayer.setText(( current_player == Constants.PLAYER_1 ) ? str_p1 : str_p2 );
			break;
		case Constants.PLAYER_2:
			
			// Display the UI appearance for the player
			// and adjust variable
			contentPane.setBackground(Constants.COLOR_P1_BKGD);
			current_player = Constants.PLAYER_1;
			lblCurrentPlayer.setText(( current_player == Constants.PLAYER_1 ) ? str_p1 : str_p2 );
			break;
		default:
			break;
		}
	}
	
	
	
	/**
	 * get_p1
	 * 
	 * Retrieves user input for player 1 name
	 * @return player 1 name input string
	 */
	
	public String get_p1() {
		
		String name = p1Field.getText();
		
		// Limit to 5 character names
		if ( name.length() > 5 )
			name = name.substring(0, 5);
		
		// Set empty names to default
		if ( name.equals("") )
			name = "Server";
		return name;
	}
	
	
	
	/**
	 * get_p2
	 * 
	 * Retrieves user input for player 2 name
	 * @return
	 */
	
	public String get_p2() {
		
		String name = p2Field.getText();
		
		/// Limit to 5 character names
		if ( name.length() > 5 )
			name = name.substring(0, 5);
		
		// Set empty names to default
		if ( name.equals("") )
			name = "Client";
		
		return name;
	}
}
