
/**
 * Box
 * 
 * Class stores the selections of various sides of a box
 * as well as the player who won the box
 * @author lanek
 *
 */

public class Box {

	public boolean left = false, right = false, top = false, bottom = false;
	int Player = Constants.NONE;
	
	public boolean select_left() {
		
		if ( left )
			return false;
		return ( left = true );
	}
	
	public boolean select_right() {
		
		if ( right )
			return false;
		return ( right = true );
	}

	public boolean select_top() {
		
		if ( top )
			return false;
		return ( top = true );
	}

	public boolean select_bottom() {
		
		if ( bottom )
			return false;
		return ( bottom = true );
	}
	
	public boolean is_full() {
		
		return left && right && top && bottom;
	}
	
	public void set_player(int player) {
		
		Player = player;
	}
	
	public int whose_box() {
		
		return Player;
	}
}
