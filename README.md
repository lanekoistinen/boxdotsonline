# BoxDot Online Game
Lane Koistinen
06.15.2020

Program creates a connection with a remote user using TCP in order to play a game.
GUI is provided to help user play.
After one game, the connection is terminated and the game closes.

GAME OBJECTIVE: See https://en.wikipedia.org/w/index.php?title=Dots_and_Boxes&oldid=1026856797

