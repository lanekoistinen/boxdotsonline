import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import javax.swing.JOptionPane;



/**
 * netThread class
 * 
 * Class manages a thread for connecting to another player
 * via TCP/IP and reading input from the user
 * @author lanek
 *
 */

public class netThread extends Thread {
	
	Socket sock;
	ServerSocket serverSock;
	String host;
	PrintWriter printer;
	NetDot dots;
	public volatile boolean connected = false;;
	
	
	
	/**
	 * netThread constructor
	 * 
	 * Constructor accepts NetDot object
	 * @param context NetDot object
	 */
	public netThread(NetDot context) {
		
		dots = context;
	}
	
	
	
	/**
	 * run
	 * 
	 * Runs the network thread -- 
	 * listens for commands and calls the class process method
	 */
	public void run() {
		
		// Make a server connection if server socket was passed
		if ( serverSock != null )
			// Accept found connections
			try {
				sock = serverSock.accept();
			} catch(Exception e) {
				
				// Close server socket and return upon error
				try {
					serverSock.close();
				} catch (Exception ignored) {}
				return;
			}
		
		// Construct a scanner using the socket input string
		Scanner scn;
		try {
			scn = new Scanner(sock.getInputStream());
		} catch (Exception e) {
			
			// Exit the entire program if the scanner fails to be created from the socket input stream
			JOptionPane.showMessageDialog(null, "Could not create scanner -- Program terminated");
			dots.trigger_exit();
			return;
		}
		
		// Construct a PrintWriter object for writing commands to the other player via the socket connection
		try {
			printer = new PrintWriter(sock.getOutputStream());
		} catch (Exception e) {
			
			// Exit the entire program if the PrintWriter object fails to be created from the socket output stream
			JOptionPane.showMessageDialog(null, "Could not establish communication -- Program terminated");
			dots.trigger_exit();
			return;
		}
		
		// Send name to connected player
		send_line(host);
		
		// Retrieve the name sent by the user
		if ( scn.hasNextLine() )
			dots.set_name(scn.nextLine());
	
		// Loop to process commands, most time should be spent in this loop
		while ( scn.hasNextLine() ) {

			String temp = scn.nextLine();
			
			// Process command
			Process(temp);
		}
		
		// Close the scanner
		scn.close();
		
		// Quit program
		send_line("Q");
		Disconnect();
		dots.trigger_exit();
	}
	
	
	
	/**
	 * send_line
	 * 
	 * Print a line to the socket input stream using
	 * PrintWriter class object
	 * @param line
	 */
	
	public synchronized void send_line(String line) {
		
		printer.println(line);
		printer.flush();
	}
	
	
	
	/**
	 * Process
	 * 
	 * Accepts string passed via socket connection
	 * and interprets info
	 * @param temp
	 */
	
	public void Process(String temp) {
		
		Scanner sc = new Scanner(temp);
		
		// Get the first letter of the command
		if (sc.hasNext())
			switch ( sc.next() ) {
			
			// Coordinate command, a click
			// has occurred
			case "C":
			case "c":
				// Retrieve the first and second coordinates
				int x, y;
				if ( sc.hasNext() )
					x = Integer.parseInt(sc.next());
				else {
					// Close scanner and return if failure to read
					// a valid command
					sc.close();
					return;
				}
				if ( sc.hasNext() )
					y = Integer.parseInt(sc.next());
				else {
					// Close scanner and return if failure to read
					// a valid command
					sc.close();
					return;
				}					
				dots.on_Click(x, y); // current_player is volatile
				sc.close();
				break;
			// Quit command
			case "Q":
			case "q":
				// Call the main class exit method
				dots.trigger_exit();
				break;
			default:
				break;
			}
	}
	
	
	
	
	/**
	 * Disconnect
	 * 
	 * Closes connections
	 * @return
	 */
	
	public boolean Disconnect() {
		
		// Close socket
		try {
			sock.close();
		} catch (Exception ignored) {}

		// Close server socket,
		// nullptr exception will be caught if player
		// is a client
		try {
			serverSock.close();
		} catch (Exception e) {
			return false;
		}
		
		// Flag a success
		return true;
	}
	
	
	
	
	/**
	 * Config
	 * 
	 * Accepts parameters to set as class variables
	 * @param ss ServerSocket (always null if client)
	 * @param hst Host for socket connection (always null if server)
	 * @param sk Socket (always null if server)
	 */
	
	public void Config(ServerSocket ss, String hst, Socket sk) {
		
		// Set params
		host = hst;
		sock = sk;
		serverSock = ss;
	}
}
