import java.awt.Color;

/**
 * Constants
 * 
 * Class stores the static parameters of the game, notably
 * for UI elements
 * @author lanek
 *
 */
public class Constants {

	public static final int NONE = 0;
	public static final int PORT = 1234;
	
	final static int height = 8;
	final static int width = 8;
	final static int dotSize = 7;
	final static int panelBorder = 1;
	final static int padding = 2 * Constants.dotSize;
	final static int lineWidth = (int) (dotSize / 2.5f);
	final static int SCORE_MULT = 10;
	
	final static Color COLOR_P1 = new Color(250,0,0,230);
	final static Color COLOR_P2 = new Color(0,0,0,10);
	final static Color COLOR_P1_BKGD = new Color(255, 250, 245);
	final static Color COLOR_P2_BKGD = new Color(230, 235, 255);
	
	public static final int PLAYER_1 = 1;
	public static final int PLAYER_2 = 2;
	public static final int BOTH = 3;
	
	public static final int BAD = -1;
	public static final int GOOD = 1;
	
	public static final int ROW = 1;
	public static final int COLUMN = 2;
}
