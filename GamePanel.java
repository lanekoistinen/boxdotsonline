import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;



/**
 * GamePanel
 * 
 * Class manipulates the game board for the Dots game
 * @author lanek
 *
 */
public class GamePanel extends JPanel {
	
	// Declare class variables
	Box[][] Boxes;	
	final int padding = Constants.padding; 
	int bw, bh = 0;
	JLabel score_1_label;
	JLabel score_2_label;
	String name_1, name_2;
	int score_1;
	int score_2;
	int Player = Constants.NONE;
	boolean new_box = false;
	
	
	
	/**
	 * Box
	 * 
	 * Static method initializes a 2D array of Box class objects
	 * 
	 * @param w boxes on x axis
	 * @param h boxes on y axis
	 * @return
	 */
	
	public static Box[][] init_2d_boxes( int w, int h ) {
		
		Box[][] boxes = new Box[h][w];
		
		for ( int r = 0; r < h; r++ )
			for ( int c = 0; c < w; c++ )
				boxes[r][c] = new Box();
		
		return boxes;
	}
	
	
	
	/**
	 * GamePanel
	 * 
	 * Constructor initializes the Boxes array and scores/names
	 * 
	 * @param str_p1 player 1 name
	 * @param str_p2 player 2 name
	 * @param sc_1 label for score 1 value
	 * @param sc_2 label for score 2 value
	 */
	
	public GamePanel(String str_p1, String str_p2, JLabel sc_1, JLabel sc_2) {
		
		Boxes = init_2d_boxes(Constants.width, Constants.height);
				
		( score_1_label = sc_1 ).setText( "" + ( score_1 = 0 ) );
		( score_2_label = sc_2 ).setText( "" + ( score_2 = 0 ) );
		
		name_1 = str_p1;
		name_2 = str_p2;
	}
	
	

	/**
	 * paint
	 * 
	 * Paints the game board based on the current Boxes array state
	 * @param g Graphics object for painting
	 */
	public void paint(Graphics g) {
		
		super.paintComponent(g);
		
		// Paint border
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		g.setColor(Color.WHITE);
		g.fillRect(Constants.panelBorder, Constants.panelBorder, this.getWidth() - 2 * Constants.panelBorder, this.getHeight() - 2 * Constants.panelBorder);
		
		// Paint the selected lines
		g.setColor(Color.BLACK);
		paintGameBoard(g);
		
		// Paint the dots
		g.setColor(Color.RED);
		paintEmptyBoard(g);
		
		// Paint the initials in filled boxes
		update_initials(g);
	}
		
	
	
	/**
	 * paintEmptyBoard
	 * 
	 * paints the dots onto the game board
	 * @param g Graphics object for painting
	 */
	
	public void paintEmptyBoard(Graphics g) {
		
		// Find the box height and width in pixels
		bw = (this.getWidth() - 2 * padding)/Constants.width;
		bh = (this.getHeight() - 2 * padding)/Constants.height;
		
		// Paint dots, iterating through the corners of each row and column
		for ( int column = 0; column < Constants.height + 1; column++ )
			for ( int row = 0; row < Constants.width + 1; row++ )
				g.fillOval(row * bw + padding - Constants.dotSize / 2, column * bh + padding - Constants.dotSize / 2, Constants.dotSize, Constants.dotSize);
	}
	
	
	
	/**
	 * paintGameBoard
	 * 
	 * Paints the user-selected lines onto the game board
	 * @param g Graphics object for painting
	 */
	
	public void paintGameBoard(Graphics g) {
		
		// Set the line width to specified constant
		Graphics2D g2D = (Graphics2D)g;
		g2D.setStroke(new BasicStroke(Constants.lineWidth));
		
		// Iterate through Boxes array to paint the selected edges of each box
		for ( int column = 0; column < Constants.width; column++ )
			for ( int row = 0; row < Constants.height; row++ ) {
				
				// Only paint when side is selected
				if ( Boxes[row][column].left )
					g2D.drawLine( bw * column + padding, bh * row + padding, bw * column + padding, bh * row + padding + bh );
				if ( Boxes[row][column].right )
					g2D.drawLine( bw * column + bw + padding, bh * row + padding, bw * column + bw + padding, bh * row + padding + bh );
				if ( Boxes[row][column].top )
					g2D.drawLine( bw * column + padding, bh * row + padding, bw * column + bw + padding, bh * row + padding );
				if ( Boxes[row][column].bottom )
					g2D.drawLine( bw * column + padding, bh * row + padding + bh, bw * column + bw + padding, bh * row + padding + bh );
			}
	}
	
	
	
	/**
	 * on_click
	 * 
	 * Updates selections based on a user click
	 * @param e click event
	 * @param player current player
	 * @return How the user input was used
	 */
	
	public int on_click(int x, int y, int player) {
		
		Player = player;
		
		// Construct selection by passing coordinates to selection method
		Selection selection = get_selection(x, y);
		
		// Check if the selection was valid
		if ( selection != null ) {
			
			if ( !use_selection(selection) )
				return Constants.NONE;
		}
		// Otherwise, report an ignored input
		else
			return Constants.NONE;
		
		// Get changes and redraw the game board
		update_score();
		repaint();
		
		// Return 'good' input if a new box was not selected
		return ( !new_box ) ? Constants.GOOD : Constants.BAD;
	}
	
	
	
	/**
	 * get_selection
	 * 
	 * Store the selection paramters in a Selection variable
	 * @param x x coordinate of mouse click
	 * @param y y coordinate of mouse click
	 * @return the Selection variable with the relevant paramters
	 */
	
	public Selection get_selection(int x, int y) {
		
		if ( bw == 0 || bh == 0 )
			return null;
		
		// Account for padding on side of graph
		x -= padding;
		y -= padding;
		
		// Ignore clicks outside of the rectangle of dots
		if ( y > bh * Constants.height || x > bw * Constants.width )
			return null;
		
		// Find the floor of the current row and column
		int flr_row = y / bh, flr_col = x / bw; 
		
		int row, column;
		int row_dist, column_dist;
		
		// Check if closer to the next column, not the float row's floor
		if ( (row_dist = y % bh) > bh / 2 ) {
			
			row_dist = bh - row_dist;
			row = flr_row + 1;
		}
		// Closer to the floor
		else
			row = flr_row;
		
		// Check if closer to the next column, not the float column's floor
		if ( (column_dist = x % bw) > bw / 2 ) {
			
			column_dist = bw - column_dist;
			column = flr_col + 1;
		}
		// Closer to the floor
		else
			column = flr_col;
		
		return new Selection(flr_row, flr_col, row, column, Math.abs(row_dist) < Math.abs(column_dist) );
	}
	
	
	
	/**
	 * use_selection
	 * 
	 * Update the Boxes array based on the new selection
	 * @param s new selection
	 * @return false if selection was already made
	 */
	
	public boolean use_selection(Selection s) {
		
		if ( s.IsRow ) {
			
			// Ignore selections beyond the final row
			// (A false condition is occasionally expected i.e. not an error)
			if ( s.closest_row < Constants.height)
				if ( !Boxes[s.closest_row][s.Flr_column].select_top() )
					return false;
			// Ignore selections before the first row
			if ( s.closest_row > 0 )
				if ( !Boxes[s.closest_row - 1][s.Flr_column].select_bottom() )
					return false;
		}
		else {
			
			// Ignore selections beyond the final column
			if ( s.closest_column < Constants.width )
				if ( !Boxes[s.Flr_row][s.closest_column].select_left() )
					return false;
			// Ignore selections before the first column
			if ( s.closest_column > 0 )
				if ( !Boxes[s.Flr_row][s.closest_column - 1].select_right() )
					return false;
		}
		return true;
	}
	
	
	
	/**
	 * update_initials
	 * 
	 * Draws the initials in filled boxes
	 * @param g Graphics object for painting
	 */
	
	public void update_initials( Graphics g ) {
		
		// Retrieve the dimensions of the labels to be drawn
		FontMetrics fm = g.getFontMetrics();
		int strW_1 = fm.stringWidth(name_1);
		int strW_2 = fm.stringWidth(name_2);
		int strH = fm.getHeight();
		int ascent = fm.getAscent();
		
		// Iterate through each box row by column
		for ( int column = 0; column < Constants.width; column++ )
			for ( int row = 0; row < Constants.height; row++ )
				// Draw the player 1 name from the class variable if player 1 is indicated
				if ( Boxes[row][column].Player == Constants.PLAYER_1 )
					g.drawString(name_1, bw * column + padding + ( bw - strW_1 ) / 2, bh * row + padding + ( bh - strH ) / 2 + ascent);
				// Draw the player 2 name from the class variable if player 2 is indicated
				else if ( Boxes[row][column].Player == Constants.PLAYER_2 ) 
					g.drawString(name_2, bw * column + padding + ( bw - strW_2 ) / 2, bh * row + padding + ( bh - strH ) / 2 + ascent);
	}
	
	
	
	/**
	 * check_winner
	 * 
	 * Update the score and check if all of the boxes are filled
	 * @return Player with the most boxes, or tie
	 */
	
	public int check_winner() {
		
		// Ensure the score is updated
		update_score();
		
		// Check if boxes are filled, return immediately if they are not
		if ( score_1 / Constants.SCORE_MULT + score_2 / Constants.SCORE_MULT < Constants.width * Constants.height)
			return Constants.NONE;
		
		// Report the winner or a tie
		if ( score_1 > score_2 )
			return Constants.PLAYER_1;
		if ( score_2 > score_1 )
			return Constants.PLAYER_2;
		else
			return Constants.BOTH;
	}
	
	
	
	/**
	 * update_score
	 * 
	 * Finds each players score and updates the score variables, as
	 * well as the displayed score
	 */
	
	public void update_score() {
		
		score_1 = score_2  = 0;
		new_box = false;		
		
		// Check the boxes array for selected boxes
		for ( int column = 0; column < Constants.width; column++ )
			
			for ( int row = 0; row < Constants.height; row++ )
				
				// Increase the player 1 score by the specified constant when box is filled by player 1
				if ( Boxes[row][column].Player == Constants.PLAYER_1 ) 					
					score_1 += Constants.SCORE_MULT;
		
				// Increase the player 2 score by the specified constant when box is filled by player
				else if ( Boxes[row][column].Player == Constants.PLAYER_2 ) 
					score_2 += Constants.SCORE_MULT;
		
				// Check if a box was just selected by the user
				else if ( Boxes[row][column].is_full() && Player != Constants.NONE ) {
					
					new_box = true;
					
					// Give the point to player 1 
					if ( Player == Constants.PLAYER_1 ) {
						
						Boxes[row][column].Player = Constants.PLAYER_1;
						score_1 += Constants.SCORE_MULT;
					}
					// Give the point to player 2
					else {
						
						Boxes[row][column].Player = Constants.PLAYER_2;
						score_2 += Constants.SCORE_MULT;
					}
				}
		
		// Update the displayed score
		score_1_label.setText("" + score_1);
		score_2_label.setText("" + score_2);
	}
	
	
	
	/**
	 * clear_game
	 * 
	 * Resets the user input for the game
	 */
	
	public void clear_game() {
		
		// Reinitialize the boxes array
		Boxes =  init_2d_boxes(Constants.width, Constants.height);
		
		repaint();
	}
	
	
	
	/**
	 * Selection class
	 * 
	 * Class stores the parameters of a user selection
	 * @author lanek
	 *
	 */
	
	private static class Selection {
		
		int Flr_row, Flr_column, closest_row, closest_column;
		boolean IsRow;
		
		/**
		 * Selection
		 * 
		 * Constructor stores the floor of the float row/column, as well as
		 * the rounded row/column
		 * @param flr_row floor of the selected row
		 * @param flr_column floor of the selected column
		 * @param row closest row
		 * @param column closest column
		 * @param isRow 
		 */
		public Selection(int flr_row, int flr_column, int row, int column, boolean isRow) {
			
			Flr_row = ( flr_row < Constants.height ) ? flr_row : Constants.height - 1;
			Flr_column = ( flr_column < Constants.width ) ? flr_column : Constants.width - 1;
			closest_row = row;
			closest_column = column;
			IsRow = isRow;
		}
	}
}
